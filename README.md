# README.md

## Environment variables

`RI-INF-HOME` Path to mount as home directory.

## Home directory

Setting the home directoy will persist config from run to run and survive rebuilding the container. Creating the following files will set the shell to run with RI defaults.

.bashrc

```bashrc
. /opt/bashrc/.bashrc
```

.inputrc

```inputrc
$include /opt/bashrc/.inputrc
```

## Shell

Access to a shell in the container from outside of VSCode.

```bash
docker exec -it -e TERM=xterm-256color ri-inf bash
```

## Dashboards of interest

* add as default dashboard
* Windows
  * <https://grafana.com/grafana/dashboards/1902> - Installed
  * <https://grafana.com/grafana/dashboards/1635>
  * <https://grafana.com/grafana/dashboards/1941>
* Influx Internals
  * <https://grafana.com/grafana/dashboards/5265>
  * <https://grafana.com/grafana/dashboards/421>
  * <https://grafana.com/grafana/dashboards/317>
  * <https://grafana.com/grafana/dashboards/11057>
  * <https://grafana.com/grafana/dashboards/7921>
  * <https://grafana.com/grafana/dashboards/10346> - Datasource has naming convention requirements: InfluxDB(_internal)
