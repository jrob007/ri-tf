alias tf=terraform
complete -C /usr/local/bin/terraform tf
complete -C /usr/local/bin/terraform terraform

alias k=kubectl
complete -F __start_kubectl k

alias h=helm
complete -F __start_helm h

alias tg=terragrunt
