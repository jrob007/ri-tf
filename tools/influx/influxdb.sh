#!/bin/bash

# https://gist.github.com/dgoguerra/9206418
# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f

# File name
readonly PROGNAME=$(basename $0)
# File name, without the extension
readonly PROGBASENAME=${PROGNAME%.*}
# File directory
readonly PROGDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
# Arguments
readonly ARGS="$@"
# Arguments number
readonly ARGNUM="$#"

usage() {
    cat <<-EOF
Script description

Usage: $PROGNAME backup -d <database> -s <server> -c <k8s-context> -f </path/to/folder>
Usage: $PROGNAME restore -d <database> -s <server> -c <k8s-context> -u <user> -p <pass> -f </path/to/folder>

Options:

    -h, --help
        This help text.

    -d <database>, --database <database>
        The database to act on.

    -s <server>, --server <server>
        The influxDB server.

    -c <context>, --context <context>
        The k8s context to use.

    -f </path/to/folder>, --folder </path/to/folder>
        The folder to save or restore data from.

    -u <username>, --username <username>
        Username for restore.

    -p <password>, --password <password>
        Password for restore.

EOF
}

action="$1"
shift

case "$action" in
    backup|restore)
        ;;
    *)
        echo "invalid action: $action"
        usage
        exit 1
esac

while [ "$#" -gt 0 ]
do
	case "$1" in
	-h|--help)
		usage
		exit 0
		;;
    -d|--database)
        database="$2"
        ;;
    -s|--server)
        server="$2"
        ;;
	-f|--folder)
		folder="$2"
		;;
    -c|--context)
		context="$2"
		;;
    -u|--username)
		username="$2"
		;;
    -p|--password)
		password="$2"
		;;
    -n|--namespace)
		password="$2"
		;;
	-*)
		echo "Invalid option '$1'. Use --help to see the valid options" >&2
		exit 1
		;;
	# an option argument, continue
	*)	;;
	esac
	shift
done

if [[ ! -v server ]];
then
    echo "No value for -s or --server."
    exit 2
fi

if [[ ! -v database ]];
then
    echo "No value for -d or --database."
    exit 2
fi

if [[ ! -v context ]];
then
    echo "No value for -c or --context."
    exit 2
fi

if [[ ! -v namespace ]];
then
    namespace=default
fi

backup() {
    kubectl --context=$context --namespace=$namespace apply -f $PROGDIR/worker.yaml
    sleep 5
    kubectl --context=$context --namespace=$namespace wait --for=condition=Ready pod/influx-backup-pod --timeout=600s

    kubectl --context=$context --namespace=$namespace exec -it influx-backup-pod -- influxd backup -database $database -portable -host $server:8088 /mnt/azure/backup/$database
    kubectl --context=$context --namespace=$namespace cp influx-backup-pod:/mnt/azure/backup/$database $folder/$database

    kubectl --context=$context --namespace=$namespace delete -f $PROGDIR/worker.yaml
}

restore() {

    # Create worker
    kubectl --context=$context --namespace=$namespace --namespace=$namespace apply -f $PROGDIR/worker.yaml
    sleep 5
    kubectl --context=$context --namespace=$namespace wait --for=condition=Ready pod/influx-backup-pod --timeout=600s

    # Copy backup to worker
    kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- mkdir -p /mnt/azure/backup/$database
    kubectl --context=$context --namespace=$namespace cp $folder/$database influx-backup-pod:/mnt/azure/backup

    if kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- curl --silent -G "http://${server}:8086/query?pretty=true" -u "${username}:${password}" --data-urlencode "q=show databases" | grep -q "$database"; then
        echo "Create database ${database}_bak and copy to $database"
        kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- influxd restore -db $database -newdb "${database}_bak" -portable -host $server:8088 /mnt/azure/backup/$database
        echo "restore data..."
        kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- curl -XPOST --silent "http://${server}:8086/query?pretty=true&db=${database}_bak" -u "${username}:${password}" --data-urlencode "q=SELECT * INTO $database..:MEASUREMENT FROM /.*/ GROUP BY *"
        echo "Drop bak database..."
        kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- curl -XPOST --silent "http://${server}:8086/query" -u "${username}:${password}" --data-urlencode "q=drop database ${database}_bak"
    else
        echo "Restore database $database"
        kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- influxd restore -db $database -portable -host $server:8088 /mnt/azure/backup/$database
    fi

    kubectl --context=$context --namespace=$namespace exec -t influx-backup-pod -- curl -G --silent "http://${server}:8086/query?pretty=true" -u "${username}:${password}" --data-urlencode "q=show databases"

    kubectl --context=$context --namespace=$namespace delete -f $PROGDIR/worker.yaml
}

$action
