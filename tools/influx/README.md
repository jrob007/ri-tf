# run and exec

```bash
kubectl apply -f worker.yaml
kubectl wait --for=condition=Ready pod/influx-backup-pod
kubectl exec -it influx-backup-pod bash
```
