remote_state {
  backend = "azurerm"
  config = {
    storage_account_name = get_env("TF_storage_account_name", "")
    container_name       = "tstate"
    key                  = "${path_relative_to_include()}/terraform.tfstate"
  }
}

terraform {
  extra_arguments "common_vars" {
    commands = [
      "destroy",
      "apply",
      "plan",
      "import",
      "push",
      "refresh"
    ]
    arguments = [
      "-var-file=${get_parent_terragrunt_dir()}/../../tf-live/secrets/${path_relative_to_include()}/terraform.tfvars"
    ]
  }
}
