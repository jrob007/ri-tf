provider "helm" {
  version = "0.10.0"
  kubernetes {
    host = "${var.k8s_host}"

    client_certificate     = "${var.k8s_client_certificate}"
    client_key             = "${var.k8s_client_key}"
    cluster_ca_certificate = "${var.k8s_cluster_ca_certificate}"
  }
}

data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

resource "helm_release" "grafana" {
  name      = "grafana-${var.environment}"
  chart     = "stable/grafana"
  version   = "3.8.19"
  namespace = "default"

  values = [
    <<-EOF
    datasources:
      datasources.yaml:
        apiVersion: 1
        datasources:
        - name: InfluxDb
          type: influxdb
          database: telegraf
          access: proxy
          user: ${var.influx_user}
          url: http://${var.influx_server}:${var.influx_port}
          jsonData:
            timeInterval: "15s"
          secureJsonData:
            password: ${var.influx_pass}
    ingress:
      enabled: true
      hosts:
        - ${var.host}
      tls:
        - secretName: grafana-tls-${var.environment}
          hosts:
          - ${var.host}
      annotations:
        kubernetes.io/ingress.class: nginx
        cert-manager.io/cluster-issuer: letsencrypt-${var.letsencrypt_environment}
    EOF
  ]

  set {
    name  = "adminUser"
    value = "${var.user}"
  }

  set {
    name  = "adminPassword"
    value = "${var.pass}"
  }

}
