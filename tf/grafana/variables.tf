variable environment {
  type = "string"
}

variable resource_group_name {
  default = "grafana"
  type    = "string"
}

variable location {
  default = "East US"
  type    = "string"
}

variable "k8s_client_key" {
  type = "string"
}

variable "k8s_client_certificate" {
  type = "string"
}

variable "k8s_cluster_ca_certificate" {
  type = "string"
}

variable "k8s_host" {
  type = "string"
}

variable "influx_user" {
  type = "string"
}

variable "influx_pass" {
  type = "string"
}

variable "influx_server" {
  type = "string"
}

variable "influx_port" {
  type = "string"
}

variable "host" {
  type = "string"
}

variable "user" {
  type = "string"
}

variable "pass" {
  type = "string"
}

variable "letsencrypt_environment" {
  type        = "string"
  default     = "staging"
  description = "staging or prod"
}
