include {
  path = find_in_parent_folders()
}

dependency "k8s" {
  config_path = "../k8s"
}

dependency "influx" {
  config_path = "../influx"
}

inputs = {
  k8s_client_key             = dependency.k8s.outputs.k8s_client_key
  k8s_client_certificate     = dependency.k8s.outputs.k8s_client_certificate
  k8s_cluster_ca_certificate = dependency.k8s.outputs.k8s_cluster_ca_certificate
  k8s_host                   = dependency.k8s.outputs.k8s_host
  influx_user                = dependency.influx.outputs.user
  influx_pass                = dependency.influx.outputs.pass
}
