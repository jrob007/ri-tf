variable name {
  type = string
}

variable environment {
  type = string
}

variable location {
  type    = string
  default = "East US"
}

variable "client_id" {
  type = string
}

variable "client_secret" {
  type = string
}

variable "git_user" {
  type = string
}

variable "git_pass" {
  type = string
}

variable "git_url" {
  type = string
}

variable "git_branch" {
  type = string
}

variable "agent_count" {
  default = 1
}

variable "acme-email" {
  type = string
}

variable "acme-issuer" {
  type    = string
  default = "letsencrypt-staging"
}

variable "domain" {
  type = string
}

variable "domain_zone_name" {
  type = string
}

variable "dns_subdomain" {
  type        = string
  description = "'*' for '*.example.com'. '*.test' for '*.test.example.com'"
}

variable "domain_resource_group" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "ssh_public_key" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable log_analytics_workspace_name {
  type    = string
  default = "testLogAnalyticsWorkspaceName"
}

# refer https://azure.microsoft.com/global-infrastructure/services/?products=monitor for log analytics available regions
variable log_analytics_workspace_location {
  type    = string
  default = "eastus"
}

# refer https://azure.microsoft.com/pricing/details/monitor/ for log analytics pricing
variable log_analytics_workspace_sku {
  type    = string
  default = "PerGB2018"
}

variable k8s_version {
  type    = string
  default = "1.15.7"
}
