resource "null_resource" "cert-manager" {
  depends_on = [null_resource.write-credentials]

  provisioner "local-exec" {
    command = "${path.module}/post-install.sh"

    environment = {
      clusterName = "${azurerm_kubernetes_cluster.k8s.name}"
      clusterRG   = "${azurerm_resource_group.rg.name}"
      path        = "${path.module}"
      email       = "${var.acme-email}"
    }
  }
}

resource "kubernetes_namespace" "flux" {
  depends_on = [null_resource.cert-manager]
  metadata {
    name = "flux"
  }
}

resource "kubernetes_namespace" "ingress-nginx" {
  depends_on = [null_resource.cert-manager]
  metadata {
    name = "ingress-nginx"
  }
}

resource "kubernetes_config_map" "ingress-values" {
  depends_on = [kubernetes_namespace.ingress-nginx]

  metadata {
    name      = "ingress-nginx-values"
    namespace = "ingress-nginx"
  }

  data = {
    "values.yaml" = <<-EOF
    controller:
      service:
        loadBalancerIP: ${azurerm_public_ip.resource.ip_address}
        annotations:
          service.beta.kubernetes.io/azure-load-balancer-resource-group: ${azurerm_resource_group.aks-ip-rg.name}
    EOF
  }
}

resource "kubernetes_config_map" "ingress-domain" {
  depends_on = [null_resource.cert-manager]

  metadata {
    name = "ingress-domain-values"
  }

  data = {
    "values.yaml" = <<-EOF
    domain: ${var.domain}
    issuer: ${var.acme-issuer}
    EOF
  }
}

resource "kubernetes_config_map" "acme-domain-email" {
  depends_on = [null_resource.cert-manager]

  metadata {
    name = "acme-domain-email"
  }

  data = {
    "values.yaml" = <<-EOF
    email: ${var.acme-email}
    EOF
  }
}

resource "kubernetes_secret" "flux-git-auth" {
  depends_on = [kubernetes_namespace.flux]

  metadata {
    name      = "flux-git-auth"
    namespace = "flux"
  }
  data = {
    GIT_AUTHUSER = "${var.git_user}"
    GIT_AUTHPASS = "${var.git_pass}"
  }
}

data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

data "helm_repository" "fluxcd" {
  name = "fluxcd"
  url  = "https://charts.fluxcd.io"
}

resource "helm_release" "flux" {
  depends_on = [kubernetes_secret.flux-git-auth]

  name          = "flux"
  chart         = "fluxcd/flux"
  namespace     = "flux"
  version       = "1.2.0"
  force_update  = "true"
  recreate_pods = "true"

  values = [
    <<-EOF
    git:
      url: https://$(GIT_AUTHUSER):$(GIT_AUTHPASS)@${var.git_url}
      branch: ${var.git_branch}
      label: flux-sync-${var.environment}
    syncGarbageCollection:
      enabled: true
    EOF
  ]

  set {
    name  = "env.secretName"
    value = "flux-git-auth"
  }
}

resource "helm_release" "helm-operator" {
  depends_on = [helm_release.flux]

  name          = "helm-operator"
  chart         = "fluxcd/helm-operator"
  namespace     = "flux"
  version       = "0.7.0"
  force_update  = "true"
  recreate_pods = "true"
  timeout       = "900"

  values = [
    <<-EOF
    workers: 1
    helm:
      versions: v3
    EOF
  ]

}
