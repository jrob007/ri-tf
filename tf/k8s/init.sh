#!/bin/bash

terraform init \
    -backend-config="storage_account_name=$TF_storage_account_name"
