output "k8s_client_key" {
  value = "${base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)}"
}

output "k8s_client_certificate" {
  value = "${base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)}"
}

output "k8s_cluster_ca_certificate" {
  value = "${base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)}"
}

output "k8s_host" {
  value = "${azurerm_kubernetes_cluster.k8s.kube_config.0.host}"
}

output "node_resource_group" {
  value = "${azurerm_kubernetes_cluster.k8s.node_resource_group}"
}
