resource "azurerm_resource_group" "rg" {
  name     = "${var.name}-${var.environment}"
  location = var.location
}

provider "kubernetes" {
  version = "1.11.1"

  host                   = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  client_key             = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)
  client_certificate     = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
}

provider "helm" {
  version = "1.0.0"

  kubernetes {
    host                   = azurerm_kubernetes_cluster.k8s.kube_config.0.host
    client_key             = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)
    client_certificate     = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
  }
}

provider "null" {
  version = "~> 2.1"
}

terraform {
  required_version = "~> 0.12"

  backend "azurerm" {
  }
}
