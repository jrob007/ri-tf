#!/bin/bash

writeIssuer(){
    name=$1
    email=$2
    server=$3
    cat <<EOF > $path/$name.yaml
apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: $name
spec:
  acme:
    server: $server
    email: $email
    privateKeySecretRef:
      name: acme-$name
    solvers:
    - http01:
        ingress:
          class: nginx
EOF
}

echo ""
date

echo ""
echo "Wait for cluster"
az aks wait --created --interval 60 --name k8s-dev --resource-group k8s-dev --timeout 1800
echo ""
date

#kubectl --namespace=kube-system wait --for=condition=Available --timeout=30m apiservices/v1beta1.metrics.k8s.io

#az aks get-credentials --overwrite-existing --name $clusterName --resource-group $clusterRG

#clusterName=k8s-dev
#path=.

echo ""
echo "Instll cert manager"
##kubectl--context=$clusterName create namespace cert-manager
kubectl --context=$clusterName apply --validate=false --filename $path/cert-manager.yaml
echo ""
date

echo ""
echo "Waiting for cert-manager"
kubectl --context=$clusterName wait --for=condition=available --timeout=600s deployment/cert-manager -n cert-manager
echo ""
date

echo ""
echo "Waiting for cert-manager-webhook"
kubectl --context=$clusterName wait --for=condition=available --timeout=600s deployment/cert-manager-webhook -n cert-manager
echo ""
date

echo ""
echo "Waiting for cert-manager-cainjector"
kubectl --context=$clusterName wait --for=condition=available --timeout=600s deployment/cert-manager-cainjector -n cert-manager
echo ""
date

echo ""
echo "Waiting for cert-manager-cainjector"
kubectl --context=$clusterName wait --for=condition=available apiservices/v1alpha2.cert-manager.io
echo ""
date

echo ""
echo "Waiting for cert-manager-cainjector"
kubectl --context=$clusterName wait --for=condition=available apiservices/v1alpha2.acme.cert-manager.io
echo ""
date

#kubectl --context=$clusterName --namespace=kube-system wait --for=condition=Available --timeout=30m apiservices/v1beta1.metrics.k8s.io

echo ""
echo "Install test resources"
kubectl --context=$clusterName apply -f $path/test-resources.yaml
kubectl --context=$clusterName wait --for=condition=ready --timeout=600s issuer/test-selfsigned --namespace cert-manager-test
kubectl --context=$clusterName wait --for=condition=ready --timeout=600s certificate/selfsigned-cert --namespace cert-manager-test
echo ""
date

kubectl --context=$clusterName delete --filename $path/test-resources.yaml

writeIssuer "letsencrypt-prod" "$email" "https://acme-v02.api.letsencrypt.org/directory"
writeIssuer "letsencrypt-staging" "$email" "https://acme-staging-v02.api.letsencrypt.org/directory"
kubectl --context=$clusterName apply --validate=false --filename $path/letsencrypt-staging.yaml
kubectl --context=$clusterName apply --validate=false --filename $path/letsencrypt-prod.yaml
kubectl --context=$clusterName wait --for=condition=ready --timeout=600s clusterissuer/letsencrypt-staging
kubectl --context=$clusterName wait --for=condition=ready --timeout=600s clusterissuer/letsencrypt-prod
rm $path/letsencrypt-prod.yaml
rm $path/letsencrypt-staging.yaml

echo ""
date

#echo ""
#echo "Delete"
#kubectl --context=$clusterName delete --filename $path/cert-manager.yaml
#date
