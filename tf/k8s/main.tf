resource "azurerm_kubernetes_cluster" "k8s" {
  depends_on = [azurerm_public_ip.resource]

  name                = "${var.name}-${var.environment}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "${var.name}-${var.environment}"
  kubernetes_version  = var.k8s_version

  linux_profile {
    admin_username = "ubuntu"

    ssh_key {
      key_data = file("${var.ssh_public_key}")
    }
  }

  default_node_pool {
    name            = "agentpool"
    node_count      = var.agent_count
    vm_size         = var.instance_type
    os_disk_size_gb = 30
    vnet_subnet_id  = azurerm_subnet.subnet.id
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }


  network_profile {
    network_plugin = "azure"
  }

  tags = {
    Environment = var.environment
  }
}

resource "azurerm_resource_group" "aks-ip-rg" {
  name     = "${var.name}-${var.environment}-ip"
  location = var.location
}

resource "azurerm_public_ip" "resource" {
  name                = "${var.name}-${var.environment}-k8s-public-ip"
  location            = var.location
  resource_group_name = azurerm_resource_group.aks-ip-rg.name
  allocation_method   = "Static"

  tags = {
    Environment = "${var.environment}"
  }
}

resource "azurerm_dns_a_record" "resource" {
  name                = var.dns_subdomain
  zone_name           = var.domain_zone_name
  resource_group_name = var.domain_resource_group
  ttl                 = 300
  records             = ["${azurerm_public_ip.resource.ip_address}"]
}

resource "null_resource" "write-credentials" {
  provisioner "local-exec" {
    command = "az aks get-credentials --overwrite-existing --name ${azurerm_kubernetes_cluster.k8s.name} --resource-group ${azurerm_resource_group.rg.name}"
  }
}
