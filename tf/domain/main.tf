resource "azurerm_dns_zone" "domain" {
  name                = "${var.domain}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
}
