
output "resource_group" {
  value = "${azurerm_resource_group.rg.name}"
}

output "zone_name" {
  value = "${var.domain}"
}
