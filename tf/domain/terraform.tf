resource "azurerm_resource_group" "rg" {
  name     = "${var.name}-${var.environment}-rg"
  location = "${var.location}"
}

terraform {
  required_version = "~> 0.12"

  backend "azurerm" {
  }
}
