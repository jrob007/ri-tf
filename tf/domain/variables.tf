variable name {
  type = "string"
}

variable environment {
  type = "string"
}

variable location {
  default = "East US"
}

variable "client_id" {
  type = "string"
}
variable "client_secret" {
  type = "string"
}

variable domain {
  type = "string"
}
