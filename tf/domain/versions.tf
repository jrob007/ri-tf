provider "azurerm" {
  version = "~> 1.36"
}

provider "random" {
  version = "~> 2.2"
}
