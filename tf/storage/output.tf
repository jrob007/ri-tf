output "influx_disk_id" {
  value = "${azurerm_managed_disk.influx_db_disk.id}"
}

output "influx_disk_size" {
  value = "${var.influx_disk_size}"
}

output "influx_disk_name" {
  value = "${azurerm_managed_disk.influx_db_disk.name}"
}
