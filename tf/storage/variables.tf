variable resource_group_name {
  default = "storage"
}

variable location {
  default = "East US"
}

variable influx_disk_size {
  default = "80"
}
