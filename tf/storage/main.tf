resource "azurerm_managed_disk" "influx_db_disk" {
  name                 = "influx_db_disk"
  resource_group_name  = "${azurerm_resource_group.rg.name}"
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "${var.influx_disk_size}"
  location             = "${var.location}"

  tags = {
    environment = "prod"
  }
}
