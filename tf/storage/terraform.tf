resource "azurerm_resource_group" "rg" {
  name     = "${var.resource_group_name}"
  location = "${var.location}"
}

terraform {
  required_version = "~> 0.12"

  backend "azurerm" {
  }
}
